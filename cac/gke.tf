module "gke" {
  source                     = "terraform-google-modules/kubernetes-engine/google"
  version                    = "0.4.0"
  project_id                 = "${var.project_id}"
  name                       = "${var.cluster_name}"
  region                     = "${var.region}"
  zones                      = "${var.zone}"
  network                    = "default"
  subnetwork                 = "default"
  ip_range_pods              = ""
  ip_range_services          = ""
  http_load_balancing        = false
  horizontal_pod_autoscaling = true
  kubernetes_dashboard       = true
  network_policy             = true

  node_pools = [
    {
      name            = "default-node-pool"
      machine_type    = "f1-micro"
      min_count       = 2 
      max_count       = 4 
      disk_size_gb    = 100
      disk_type       = "pd-standard"
      image_type      = "COS"
      auto_repair     = true
      auto_upgrade    = true
      service_account = "${var.gke-service-account}@${var.project_id}.iam.gserviceaccount.com"
      preemptible     = false
    },
  ]

 
  node_pools_labels = {
    all = {}

    default-node-pool = {
      default-node-pool = "true"
    }
  }

  node_pools_taints = {
    all = []

    default-node-pool = [
      {
        key    = "default-node-pool"
        value  = "true"
        effect = "PREFER_NO_SCHEDULE"
      },
    ]
  }

  node_pools_tags = {
    all = []

    default-node-pool = [
      "default-node-pool",
    ]
  }
}

