data "template_file" "startup-script" {
  template = "${file("${format("%s/scripts/apache2.sh.tpl", path.module)}")}"

  vars {
    // .. nothing yet
  }
}

module "linux_app_server" {
  source                 = "GoogleCloudPlatform/managed-instance-group/google"
  version                = "1.1.14"
  name                   = "linux-application-server"
  service_port_name      = "linux-app-service-port"
  size                   = "1"
  machine_type           = "f1-micro"
  service_port           = "80"
  zonal                  = "false"
  disk_type              = "pd-standard"
  disk_size_gb           = "20"
  region                 = "${var.region}"
  access_config          = "${var.no_external_ip_access_config}"
  compute_image          = "projects/debian-cloud/global/images/family/debian-9"
  wait_for_instances     = "true"
  target_tags            = ["${module.nat.routing_tag_regional}"]
  service_account_email  = "${var.compute_sa_name}@${var.project_id}.iam.gserviceaccount.com"
  http_health_check      = "false"
  startup_script         = "${data.template_file.startup-script.rendered}"
}
module "linux_http_server_lb" {
  source = "GoogleCloudPlatform/lb-http/google"
  version = "1.0.3"
  name = "linux-http-server-lb"
  region = "${var.region}"
  target_tags = ["${module.linux_app_server.target_tags}"]
    backends = {
      "0" = [
        {
         group = "${module.linux_app_server.region_instance_group}"
        }
     ],
     }
       backend_params    = [
    # health check path, port name, port number, timeout seconds.
    "/,http,80,10"
  ]
}
