pt-get update
apt-get install -y apache2
cat <<EOF > /var/www/html/index.html
<html><body><h1>Hello World</h1>
<p>This page was created from a simple startup script!</p>
</body></html>
EOF

# Also install chefdk
/usr/bin/curl -L https://omnitruck.chef.io/install.sh | sudo bash -s -- -c stable -P chefdk -v 3.5.13
#wget  https://packages.chef.io/files/stable/chefdk/3.2.30/ubuntu/18.04/chefdk_3.2.30-1_amd64.deb
#dpkg -i chefdk_3.2.30-1_amd64.deb

mkdir -p /var/chef /etc/chef /home/chef/.berkshelf
export HOME=/home/chef
# configure Berksfile for recepie sources
cat > /home/chef/Berksfile << EOL
source "https://supermarket.chef.io"
cookbook "nginx"
EOL
# configure settings for chef-solo to work
cat > /var/chef/runbook.json << EOL
{ "run_list" : ["recipe[nginx]"]}
EOL
cat >  /etc/chef/client.rb << EOL
override_attribute_whitelist = {}
EOL
ddcat 
cd /home/chef
cat Berksfile
# Download the cookbook dependencies
berks vendor ./cookbooks --berksfile Berksfile --debug
echo running chef 
chef-solo -j /var/chef/runbook.json
